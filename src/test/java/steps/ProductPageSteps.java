package steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pages.HomePage;
import pages.ProductPage;

public class ProductPageSteps {
    private ProductPage productPage;
    private HomePage homePage;



    @Step
    public void addToShoppingCart(){
        productPage.addToShoppingCart();
    }
    @Step
    public void checkPrice(){
        productPage.checkPrice();
    }
    @Step
    public void checkProductDescription(){
        productPage.checkProductDescription();
    }
    @Step
    public void verifyProductIsAdded(){
        productPage.verifyProductAdded();
    }
}
