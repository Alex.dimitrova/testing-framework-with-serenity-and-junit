package steps;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import pages.HomePage;
import pages.ProductPage;
import pages.ShoppingCartPage;

public class ShoppingCartSteps {
    private ProductPage productPage;
    private HomePage homePage;
    private ShoppingCartPage shoppingPage;

    @Step
    public void openCart(){
        shoppingPage.openShoppingCart();
    }
    @Step
    public void deleteItemFromCart(){
        shoppingPage.deleteProductFromCard();
    }
    @Step
    public void verifyItemIsDeleted(){
        shoppingPage.verifyProductIsDeleted();
    }

    @Step
    public void placeOrder(){
        shoppingPage.placeOrder();
    }
    @Step
    public void verifyOrder(){
        shoppingPage.verifyOrderIsPlaced();
    }
    @Step
    public void cancelOrder(){
        shoppingPage.cancelTheOrderForm();
    }
}
