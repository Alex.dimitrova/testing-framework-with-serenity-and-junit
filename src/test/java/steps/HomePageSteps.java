package steps;

import net.thucydides.core.annotations.Step;
import pages.HomePage;

public class HomePageSteps {

    private HomePage homePage;

    @Step
    public void openWebPage(){
        homePage.open();

    }
    @Step
    public void createAnAccount(){
        homePage.SignUp();
    }
    @Step
    public void verifyAccountCreated(){
        homePage.verifyAccountIsCreated();
    }
    @Step
    public void logIn(){
        homePage.logIn();
    }
    @Step
    public void verifyUserIsLogged(){
        homePage.verifyUserIsLogged();

    }
    @Step
    public void findAProduct(){
        homePage.findAProduct();
    }
    @Step
    public void selectAProduct(){
        homePage.selectAProduct();
    }
    @Step
    public void openContactForm(){
        homePage.openContactsForm();
    }
    @Step
    public void submitTheContactForm(){
        homePage.submitTheContactForm();
    }
    @Step
    public void verifyMessageIsSent(){
        homePage.verifyMessageIsSent();
    }
    @Step
    public void logOut(){
        homePage.logOut();
    }
}
