package tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.ProductPageSteps;
import steps.ShoppingCartSteps;

@RunWith(SerenityRunner.class)
public class ShoppingCartPageTests {
    @Steps
    ProductPageTests productPageTests;
    @Steps
    ProductPageSteps productPageSteps;

    @Steps
    ShoppingCartSteps shoppingCart;

    @Managed(driver = "firefox")
    WebDriver browser;

    @Test
    @Title("When a user has added an item to his shopping card he should be able to then remove it")
    public void deleteItemFromCart(){
        productPageTests.addProductToCart();
        productPageSteps.verifyProductIsAdded();
        shoppingCart.openCart();
        shoppingCart.deleteItemFromCart();
        shoppingCart.verifyItemIsDeleted();
    }
}
