package tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.HomePageSteps;
import steps.ProductPageSteps;

@RunWith(SerenityRunner.class)
public class ProductPageTests {

    @Steps
    ProductPageSteps productPage;
    @Steps
    HomePageSteps homePage;

    @Managed(driver = "firefox")
    WebDriver browser;

    @Test
    @Title("Registered users can successfully add a product to the shopping card")
    public void addProductToCart() {
        homePage.openWebPage();
        homePage.logIn();
        homePage.findAProduct();
        homePage.selectAProduct();
        productPage.checkPrice();
        productPage.checkProductDescription();
        productPage.addToShoppingCart();
    }
}
