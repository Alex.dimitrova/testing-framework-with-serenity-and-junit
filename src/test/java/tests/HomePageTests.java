package tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.HomePageSteps;


@RunWith(SerenityRunner.class)
public class HomePageTests {
    @Steps
    HomePageSteps homePage;

    @Managed()
    WebDriver browser;

    @Test
    @Title("User should be able to create an account")
    public void signUp(){
        homePage.openWebPage();
        homePage.createAnAccount();
        homePage.verifyAccountCreated();
        browser.close();
    }
    @Test
    @Title("Registered users can successfully log in")
    public void logIn(){
        homePage.openWebPage();
        homePage.logIn();
        homePage.verifyUserIsLogged();
        browser.close();
    }

    @Test
    @Title("Users can successfully send a message through the contact form")
    public void sendAMessage(){
        homePage.openWebPage();
        homePage.openContactForm();
        homePage.submitTheContactForm();
    }
}