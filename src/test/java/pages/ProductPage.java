package pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import static org.junit.Assert.*;


@DefaultUrl("https://www.demoblaze.com/")
public class ProductPage extends PageObject {

    public void checkPrice(){
        String productPrice = "$360";
        assertEquals(productPrice, "$360");
    }

    public void checkProductDescription(){
        String productDescription = "The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos 7420 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage cannot be expanded. ";
        assertEquals(productDescription, "The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos 7420 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage cannot be expanded. ");
    }


    public void addToShoppingCart() {
        $(By.linkText("Add to cart")).click();
    }
    public void verifyProductAdded(){
        waitABit(2000);
        getAlert().accept();

    }
}
