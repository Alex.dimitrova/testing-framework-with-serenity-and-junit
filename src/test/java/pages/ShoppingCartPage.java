package pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class ShoppingCartPage extends PageObject {

    String samsung = "//td[contains(.,'Samsung galaxy s6')]";
    public void openShoppingCart() {
        $(By.linkText("Cart")).click();
        waitForRenderedElementsToBePresent(By.id("tbodyid"));
        shouldBeVisible(By.xpath(samsung));
    }

    public void deleteProductFromCard() {
        //$(By.linkText("Delete")).click();
        do {
            $(By.linkText("Delete")).click();
            waitABit(2000);
        }
        while ($(By.xpath(samsung)).isVisible());
    }
    public void verifyProductIsDeleted(){
        waitForAbsenceOf("//td[contains(.,'Samsung galaxy s6')]");
        shouldNotBeVisible(By.xpath("//td[contains(.,'Samsung galaxy s6')]"));
    }

    public void placeOrder() {

        waitForRenderedElementsToBePresent((By.xpath("//button[@type='button'][contains(.,'Place Order')]")));
        $(By.xpath("//button[@type='button'][contains(.,'Place Order')]")).click();
        $(By.id("name")).type("Test User");
        $(By.id("country")).type("Bulgaria");
        $(By.id("city")).type("Sofia");
        $(By.id("card")).type("1234545455");
        $(By.id("month")).type("03");
        $(By.id("year")).type("2020");
        $(By.xpath("//button[@type='button'][contains(.,'Purchase')]")).click();

    }
    public void verifyOrderIsPlaced(){
        shouldContainText("Thank you for your purchase!");
    }

    public void cancelTheOrderForm() {
        waitForRenderedElementsToBePresent((By.xpath("//button[@type='button'][contains(.,'Place Order')]")));
        $(By.xpath("//button[@type='button'][contains(.,'Place Order')]")).click();
        $(By.id("name")).type("Test User");
        $(By.id("country")).type("Bulgaria");
        $(By.id("city")).type("Sofia");
        $(By.id("card")).type("1234545455");
        $(By.id("month")).type("03");
        $(By.id("year")).type("2020");
        $(By.xpath("(//button[contains(.,'Close')])[3]")).click();

    }
    }