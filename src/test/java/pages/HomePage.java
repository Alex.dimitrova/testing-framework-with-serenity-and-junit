package pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;


@DefaultUrl("https://www.demoblaze.com/")
public class HomePage extends PageObject {
    String email = generateRandomEmail("mail.com", 8);

    public void SignUp(){
        $(By.id("signin2")).click();
        $(By.id("sign-username")).type(email);
        $(By.id("sign-password")).type("164845");
        $(By.xpath("//button[@type='button'][contains(.,'Sign up')]")).click();

    }
    public void verifyAccountIsCreated(){
        getAlert().accept();
    }

    public void logIn(){
        $(By.id("login2")).click();
        $(By.id("loginusername")).type("tes2@user.com");
        $(By.id("loginpassword")).type("164845");
        $(By.xpath("//button[@type='button'][contains(.,'Log in')]")).click();
    }
    public void verifyUserIsLogged(){
        assertTrue("Welcome tes2@user.com", true);

    }

    public String generateRandomEmail(String domain, int length) {
        return RandomStringUtils.random(length, "abcdefghijklmnopqrstuvwxyz") + "@" + domain;

    }
    public void findAProduct(){
        shouldBeVisible(By.linkText("Samsung galaxy s6"));

    }
    public void selectAProduct(){
        $(By.linkText("Samsung galaxy s6")).click();
        shouldBeVisible(By.linkText("Add to cart"));

    }
    public void openContactsForm(){
        $(By.linkText("Contact")).click();
        assertTrue("New message", true);

    }
    public void submitTheContactForm(){
        $(By.id("recipient-email")).type("test@mail.com");
        $(By.id("recipient-name")).type("goshko");
        $(By.id("message-text")).type("send help");
        $(By.xpath("//button[@type='button'][contains(.,'Send message')]")).click();

    }
    public void verifyMessageIsSent(){
        waitABit(2000);
        getAlert().accept();
    }
    public void logOut(){
        waitABit(2000);
        $(By.xpath("//a[@class='nav-link'][contains(.,'Log out')]")).click();
        //$(By.id("logout2")).click();
        shouldBeVisible(By.id("signin2"));
    }
}
