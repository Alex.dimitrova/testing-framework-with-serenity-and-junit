package End2EndTests;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.ProductPageSteps;
import steps.ShoppingCartSteps;
import tests.ProductPageTests;

@RunWith(SerenityRunner.class)
public class PlaceOrderTest {

        @Steps
        ProductPageSteps productPage;
        @Steps
        ProductPageTests productPageTests;
        @Steps
        ShoppingCartSteps shoppingSteps;

        @Before
        public void addToCart(){
            productPageTests.addProductToCart();
        }

        @Managed(driver = "firefox")
        WebDriver browser;

        @Test
        @Title("A user should be able to successfully login, add a product to his card and place an order")
        public void placeAnOrder() {
            productPage.verifyProductIsAdded();
            shoppingSteps.openCart();
            shoppingSteps.placeOrder();
            shoppingSteps.verifyOrder();

        }
}
