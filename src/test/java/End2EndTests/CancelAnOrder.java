package End2EndTests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.HomePageSteps;
import steps.ProductPageSteps;
import steps.ShoppingCartSteps;
import tests.ProductPageTests;

@RunWith(SerenityRunner.class)
public class CancelAnOrder {
    @Steps
    ProductPageSteps productPage;
    @Steps
    ProductPageTests productPageTests;
    @Steps
    ShoppingCartSteps shoppingSteps;
    @Steps
    HomePageSteps homePageSteps;
    @Before
    public void addToCart(){
        productPageTests.addProductToCart();
    }

    @Managed(driver = "firefox")
    WebDriver browser;

    @Test
    @Title("A user should be able to successfully login, add a product to his card and cancel the order form if he's change his mind")
    public void cancelAnOrder(){
        productPage.verifyProductIsAdded();
        shoppingSteps.openCart();
        shoppingSteps.cancelOrder();
        shoppingSteps.deleteItemFromCart();
        homePageSteps.logOut();
    }
}
